import { Component } from '@angular/core';

@Component({
  selector: 'ns-root',
  templateUrl: './app.component.html',
  styles: []
})
export class AppComponent {
  title = 'nearby-shops-ihm';
}
