import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AUTH_ROUTES } from './auth.routes';
import { LoginComponent } from './components/login/login.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { WrapperComponent } from './components/wrapper/wrapper.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(AUTH_ROUTES)
  ],
  declarations: [LoginComponent, SignUpComponent, WrapperComponent]
})
export class AuthModule { }
