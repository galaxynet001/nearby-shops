import { Routes } from "@angular/router";
import { WrapperComponent } from "./components/wrapper/wrapper.component";
import { LoginComponent } from "./components/login/login.component";
import { SignUpComponent } from "./components/sign-up/sign-up.component";

export const AUTH_ROUTES: Routes = [
    { path: '', component: WrapperComponent, children: [
        { path: 'login', component: LoginComponent },
        { path: 'register', component: SignUpComponent },
        { path: '**', redirectTo: 'login', pathMatch: 'full' }
    ]},
    { path: '**', redirectTo: '/', pathMatch: 'full' }
];
