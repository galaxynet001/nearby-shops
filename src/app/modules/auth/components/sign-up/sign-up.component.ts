import { Component, OnInit } from '@angular/core';
import { SignUpForm } from '../../model/sign-up-form.model';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'ns-sign-up',
  templateUrl: './sign-up.component.html',
  styles: []
})
export class SignUpComponent implements OnInit {

  registerForm: any = {};
  isSignedUp = false;
  isSignUpFailed = false;
  errorMessage = '';

  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  public onSubmit(): void {
    console.log(this.registerForm);
    this.userService.signUp(this.registerForm).subscribe(
      data => {
        console.log(data);
        this.isSignedUp = true;
        this.isSignUpFailed = false;
      },
      error => {
        console.log(error);
        this.errorMessage = error.message;
        this.isSignUpFailed = true;
      }
    );
  }

}
