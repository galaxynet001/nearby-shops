import { Component, OnInit } from '@angular/core';

declare var $: any;

@Component({
  selector: 'ns-wrapper',
  templateUrl: './wrapper.component.html',
  styles: []
})
export class WrapperComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('body').attr('class', 'login-page');
  }

}
