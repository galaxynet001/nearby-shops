import { Injectable } from '@angular/core';
import { CanLoad, Route, Router } from '@angular/router';
import { UserService } from '../services/user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad {

  constructor(private userService: UserService, private router: Router) {
  }

  canLoad(route: Route): boolean {
    if (!this.userService.isLoggedIn()) {
      this.router.navigate(['/auth/login']);
      return false;
    }
    return true;
  }
}
