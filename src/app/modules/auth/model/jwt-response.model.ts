export interface JwtResponse {
    token: string;
    accessToken: string;
    username: string;
}