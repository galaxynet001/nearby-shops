import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginForm } from '../model/login-form.model';
import { JwtResponse } from '../model/jwt-response.model';
import { SignUpForm } from '../model/sign-up-form.model';
import { ResponseMessage } from 'src/app/model/response-message.model';
import { TokenStorageService } from './token-storage.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private loginUrl = 'http://localhost:5000/api/auth/signin';
  private signupUrl = 'http://localhost:5000/api/auth/signup';

  constructor(private http: HttpClient,
            private token: TokenStorageService,
            private router: Router) { }

  public isLoggedIn(): boolean {
    return this.token.getToken() != null && this.token.getToken().length > 1;
  }

  // JwtResponse(accessToken,type,username)
  public attemptAuth(credentials: LoginForm): Observable<JwtResponse> {
    return this.http.post<JwtResponse>(this.loginUrl, credentials);
  }
 
  // SignUpInfo(name,username,email,password)
  public signUp(info: SignUpForm): Observable<ResponseMessage> {
    return this.http.post<ResponseMessage>(this.signupUrl, info);
  }

  // SignUpInfo(name,username,email,password)
  public logout(): void {
    this.token.signOut();
    this.router.navigate(['/auth/login']);
  }
}
