import { Component, OnInit } from '@angular/core';
import { Shop } from '../../model/shop.model';
import { ShopService } from '../../services/shop.service';

@Component({
  selector: 'ns-preferred-list',
  templateUrl: './preferred-list.component.html',
  styles: []
})
export class PreferredListComponent implements OnInit {

  shops: Shop[] = [];
  
  constructor(private shopService: ShopService) { }

  ngOnInit() {
    this.shopService.loadPreferredShops().subscribe(res => {
      this.shops = res;
    });
  }

  remove(id: number): void {
    this.shopService.unlike(id).subscribe( res => this.shops = res);
  }
}
