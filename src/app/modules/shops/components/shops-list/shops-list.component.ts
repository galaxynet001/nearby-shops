import { Component, OnInit } from '@angular/core';
import { Shop } from '../../model/shop.model';
import { ShopService } from '../../services/shop.service';

@Component({
  selector: 'ns-shops-list',
  templateUrl: './shops-list.component.html',
  styles: []
})
export class ShopsListComponent implements OnInit {

  shops: Shop[] = [];
  
  constructor(private shopService: ShopService) { }

  ngOnInit() {
    this.shopService.loadShops().subscribe(res => {
      this.shops = res;
    });
  }

  dislike(id: number): void {
    this.shopService.dislike(id).subscribe( res => this.shops = res);
  }

  like(id: number): void {
    this.shopService.like(id).subscribe( res => this.shops = res);
  }
}
