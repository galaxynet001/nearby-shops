export interface Shop {
    title: string;
    image: any;
    status: number;
}