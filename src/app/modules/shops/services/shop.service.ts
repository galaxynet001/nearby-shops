import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Shop } from '../model/shop.model';

@Injectable()
export class ShopService {

  private shopsUrl = 'http://localhost:5000/api/shops';

  constructor(private http: HttpClient) {
  }

  public loadShops(): Observable<Shop[]> {
    return this.http.get<Shop[]>(this.shopsUrl);
  }

  public loadPreferredShops(): Observable<Shop[]> {
    return this.http.get<Shop[]>(`${this.shopsUrl}?liked=1`);
  }

  public like(id: number): Observable<Shop[]> {
    return this.http.get<Shop[]>(`${this.shopsUrl}/${id}/like`);
  }

  public dislike(id: number): Observable<Shop[]> {
    return this.http.get<Shop[]>(`${this.shopsUrl}/${id}/dislike`);
  }

  public unlike(id: number): Observable<Shop[]> {
    return this.http.delete<Shop[]>(`${this.shopsUrl}/${id}/like`);
  }
}
