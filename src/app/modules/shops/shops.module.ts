import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SHOPS_ROUTES } from './shops.routes';
import { PreferredListComponent } from './components/preferred-list/preferred-list.component';
import { ShopService } from './services/shop.service';
import { ShopsListComponent } from './components/shops-list/shops-list.component';
import { HomeComponent } from './components/home/home.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(SHOPS_ROUTES)
  ],
  declarations: [
    ShopsListComponent,
    PreferredListComponent,
    HomeComponent
  ],
  providers: [
    ShopService
  ]
})
export class ShopsModule { }
