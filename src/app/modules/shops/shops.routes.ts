import { Routes } from "@angular/router";
import { HomeComponent } from "./components/home/home.component";
import { PreferredListComponent } from "./components/preferred-list/preferred-list.component";
import { ShopsListComponent } from "./components/shops-list/shops-list.component";

export const SHOPS_ROUTES: Routes = [
    { path: '', component: HomeComponent, children: [
        { path: '', component: ShopsListComponent },
        { path: 'preferred-list', component: PreferredListComponent },
        { path: '**', redirectTo: '', pathMatch: 'full' }
    ]},
    { path: '**', redirectTo: '', pathMatch: 'full' }
];
